# Debt Transparency Platform Delivery

This project aims to deliver:

- a digital platform to receive and disseminate data on forms sovereign debt of low and emerging market countries;
- a process for lending entities to report in a transparent and user-friendly manner, which is a key principle of the IIF principles;
- an environment for carrying out analysis and report on data trends and implications.

The *Platform delivery* extends over the five phases of the overall Data Management and Reporting for the IIF Voluntary Principles for Debt Transparency project, **starting January 2021 through to March 2022**.

**Phase I (aka “Discovery”)**, and the phase under which the blueprint has been drafted,  consisted of the discovery phase engaging with the users of the Platform through interviews and focus group workshops, in order to develop iteratively and bring about the useful features and services needed. 

**Phase II (aka “Alpha”)**, consists in building prototype(s) of key features and testing the key ideas collected during phase I. This is to help better collect test data, and to test the use of the aggregated data (by country and by instrument) with selected intended users, to incorporate feedback and refine the Blueprint for the Platform features before the “beta” launch in phase III.

**Phase III (aka “Beta”)**, will deliver the first complete user experience, or “beta” launch, to a selected set of users (reporting entities and data users).

**Phase IV (aka “Live”)** will move forward to publicly launch the full Platform for use by all potential users, who can see and download detailed data, and communicate the launch and assessment, and to fully assess the debt levels and trends; and to communicate the launch and findings. This phase will include refinement of the Platform interface and data displays, making it the first production-grade “live” version of the Platform.

Further refinement of the Platform, based on user and stakeholder feedback, will continue through to **phase V (aka “live+”)**. 


Here is the architecture and workflow diagram of the platform
![image](/uploads/cdc9f879b39dfdab7d0e8f524e93eb36/image.png)[IIF_import_process.drawio](/uploads/a28c085484b17b9ab0bcf8b4f38d4ab3/IIF_import_process.drawio)
